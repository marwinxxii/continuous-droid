package com.cidroid.core;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import com.cidroid.core.entities.ProjectType;

public abstract class Extras {
    private static final String EXTRA_URL = "url", EXTRA_PROJECT_TYPE = "projectType";

    public static String getUrl(Activity activity) {
        return activity.getIntent().getStringExtra(EXTRA_URL);
    }

    public static Intent putUrl(Intent intent, String url) {
        intent.putExtra(EXTRA_URL, url);
        return intent;
    }

    public static ProjectType getProjectType(Activity activity) {
        return getProjectType(activity.getIntent().getExtras());
    }

    public static ProjectType getProjectType(Bundle bundle) {
        return Enum.valueOf(ProjectType.class, getProjectTypeString(bundle));
    }

    private static String getProjectTypeString(Bundle bundle) {
        return bundle.getString(EXTRA_PROJECT_TYPE);
    }

    public static ProjectType getProjectType(Fragment fragment) {
        return getProjectType(fragment.getArguments());
    }

    public static Bundle putProjectType(Bundle bundle, ProjectType projectType) {
        bundle.putString(EXTRA_PROJECT_TYPE, projectType.name());
        return bundle;
    }

    public static Intent putProjectType(Intent intent, ProjectType projectType) {
        intent.putExtra(EXTRA_PROJECT_TYPE, projectType.name());
        return intent;
    }
}

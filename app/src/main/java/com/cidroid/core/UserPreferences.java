package com.cidroid.core;

import android.content.Context;
import android.content.SharedPreferences;
import com.google.common.base.Joiner;

import javax.annotation.Nullable;
import java.util.HashSet;
import java.util.Set;

public final class UserPreferences {
    public static final long DEFAULT_BUILD_STATUS_CHECK_INTERVAL = 15000L;
    public static final int DEFAULT_BUILD_REQUEST_MAX_RETRIES = 3;
    private static final String PREFS_FILE = "cidroid";

    private final SharedPreferences mSharedPreferences;

    public UserPreferences(Application application) {
        this(application.getApplicationContext());
    }

    public UserPreferences(Context context) {
        mSharedPreferences = context.getApplicationContext()
          .getSharedPreferences(PREFS_FILE, Context.MODE_PRIVATE);
    }

    public int getBuildRequestMaxRetries() {
        return getInt(Pref.BUILD_REQUEST_MAX_RETRIES, DEFAULT_BUILD_REQUEST_MAX_RETRIES);
    }

    private int getInt(Pref pref, int defaultValue) {
        return mSharedPreferences.getInt(pref.name(), defaultValue);
    }

    public long getBuildCheckInterval() {
        return getLong(Pref.BUILD_STATUS_CHECK_INTERVAL, DEFAULT_BUILD_STATUS_CHECK_INTERVAL);
    }

    private long getLong(Pref pref, long defaultValue) {
        return mSharedPreferences.getLong(pref.name(), defaultValue);
    }

    public boolean shouldSaveLastServerName() {
        return getBoolean(Pref.ADD_SAVE_SERVER, true);
    }

    private boolean getBoolean(Pref pref, boolean defaultValue) {
        return mSharedPreferences.getBoolean(pref.name(), defaultValue);
    }

    @Nullable
    public String getLastServerName() {
        return getStringOrNull(Pref.ADD_SERVER_NAME);
    }

    private String getStringOrNull(Pref pref) {
        return mSharedPreferences.getString(pref.name(), null);
    }

    public void saveServerName(String serverName) {
        saveString(Pref.ADD_SERVER_NAME, serverName);
    }

    private void saveString(Pref pref, String value) {
        getEditor().putString(pref.name(), value).commit();
    }

    private SharedPreferences.Editor getEditor() {
        return mSharedPreferences.edit();
    }

    public void addWidgetIds(int[] widgetIds) {
        Set<Integer> savedIds = getWidgetsIdSet();
        for (int id : widgetIds) {
            savedIds.add(id);
        }
        saveWidgetIds(savedIds);
    }

    private Set<Integer> getWidgetsIdSet() {
        HashSet<Integer> result = new HashSet<>();
        int[] ids = getWidgetIds();
        if (ids != null) {
            for (int id : ids) {
                result.add(id);
            }
        }
        return result;
    }

    private void saveWidgetIds(Set<Integer> widgetIds) {
        saveString(Pref.WIDGETS_LIST, Joiner.on(',').join(widgetIds));
    }

    @Nullable
    public int[] getWidgetIds() {
        String idString = getStringOrNull(Pref.WIDGETS_LIST);
        if (idString == null) {
            return null;
        }
        String[] ids = idString.split(",");
        int[] result = new int[ids.length];
        for (int i = 0; i < ids.length; i++) {
            result[i] = Integer.parseInt(ids[i]);
        }
        return result;
    }

    public void deleteWidgetIds(int[] deletedIds) {
        Set<Integer> savedIds = getWidgetsIdSet();
        for (int id : deletedIds) {
            savedIds.remove(id);
        }
        saveWidgetIds(savedIds);
    }

    private static enum Pref {
        BUILD_REQUEST_MAX_RETRIES,
        BUILD_STATUS_CHECK_INTERVAL,

        ADD_SAVE_SERVER,
        ADD_SERVER_NAME,

        WIDGETS_LIST,
    }
}

package com.cidroid.core;

import rx.Observable;
import rx.Subscriber;
import rx.functions.Func0;

import java.util.concurrent.Callable;

public abstract class RxHelper {

    public static <T> Observable<T> createObservable(final Callable<T> func) {
        return Observable.create(new Observable.OnSubscribe<T>() {
            @Override
            public void call(Subscriber<? super T> subscriber) {
                try {
                    subscriber.onNext(func.call());
                    subscriber.onCompleted();
                } catch (Throwable e) {
                    subscriber.onError(e);
                }
            }
        });
    }

    public static <T> Observable<T> fromLazyIterable(final Func0<Iterable<T>> func) {
        return Observable.create(new Observable.OnSubscribe<T>() {
            @Override
            public void call(Subscriber<? super T> subscriber) {
                try {
                    Iterable<T> items = func.call();
                    for (T item : items) {
                        if (subscriber.isUnsubscribed()) {
                            break;
                        }
                        subscriber.onNext(item);
                    }
                    subscriber.onCompleted();
                } catch (Exception e) {
                    subscriber.onError(e);
                }
            }
        });
    }
}

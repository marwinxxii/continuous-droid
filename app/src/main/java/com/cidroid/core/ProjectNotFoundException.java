package com.cidroid.core;

public class ProjectNotFoundException extends RuntimeException {
    private String mProjectName;

    public ProjectNotFoundException() {
    }

    public ProjectNotFoundException(String detailMessage) {
        super(detailMessage);
    }

    public ProjectNotFoundException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public ProjectNotFoundException(Throwable throwable) {
        super(throwable);
    }

    public String getProjectName() {
        return mProjectName;
    }

    public ProjectNotFoundException setProjectName(String projectName) {
        mProjectName = projectName;
        return this;
    }
}

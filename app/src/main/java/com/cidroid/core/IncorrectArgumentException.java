package com.cidroid.core;

public class IncorrectArgumentException extends LocalizableRuntimeException {
    public IncorrectArgumentException() {
    }

    public IncorrectArgumentException(String detailMessage) {
        super(detailMessage);
    }

    public IncorrectArgumentException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public IncorrectArgumentException(Throwable throwable) {
        super(throwable);
    }
}

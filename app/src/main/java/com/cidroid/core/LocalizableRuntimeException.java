package com.cidroid.core;

//TODO do not pass resource ids, use different exceptions?
public class LocalizableRuntimeException extends RuntimeException {
    private int mMessageResId = -1;

    public LocalizableRuntimeException() {
    }

    public LocalizableRuntimeException(String detailMessage) {
        super(detailMessage);
    }

    public LocalizableRuntimeException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public LocalizableRuntimeException(Throwable throwable) {
        super(throwable);
    }

    public boolean hasMessageResId() {
        return mMessageResId != -1;
    }

    public int getMessageResId() {
        return mMessageResId;
    }

    public LocalizableRuntimeException setMessageResId(int messageResId) {
        mMessageResId = messageResId;
        return this;
    }
}

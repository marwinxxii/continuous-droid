package com.cidroid.core;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class DownloadReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent serviceIntent = BuilderService.getStartInstallIntent(context, intent);
        context.startService(serviceIntent);
    }
}

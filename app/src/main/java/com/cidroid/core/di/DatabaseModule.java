package com.cidroid.core.di;

import com.cidroid.core.Application;
import com.cidroid.core.db.DbOpenHelper;
import com.cidroid.core.db.ObservableDb;
import dagger.Module;
import dagger.Provides;

import javax.inject.Singleton;

@Module
public class DatabaseModule {
    @Provides
    @Singleton
    DbOpenHelper provideDbHelper(Application application) {
        return DbOpenHelper.getHelper(application.getApplicationContext());
    }

    @Provides
    @Singleton
    ObservableDb provideObservableDb(DbOpenHelper dbOpenHelper) {
        return new ObservableDb(dbOpenHelper);
    }
}

package com.cidroid.core.di;

import com.cidroid.core.net.ObservableHttpClient;
import com.squareup.okhttp.OkHttpClient;
import dagger.Module;
import dagger.Provides;

import javax.inject.Singleton;

@Module
public class NetworkModule {
    @Provides
    @Singleton
    OkHttpClient provideHttpClient() {
        return new OkHttpClient();
    }

    @Provides
    @Singleton
    ObservableHttpClient provideObservableHttpClient(OkHttpClient httpClient) {
        return new ObservableHttpClient(httpClient);
    }
}

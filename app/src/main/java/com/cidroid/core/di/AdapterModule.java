package com.cidroid.core.di;

import com.cidroid.adapters.AdapterFactory;
import com.cidroid.core.net.ObservableHttpClient;
import dagger.Module;
import dagger.Provides;

import javax.inject.Singleton;

@Module
public final class AdapterModule {
    @Provides
    @Singleton
    AdapterFactory provideFactory(ObservableHttpClient httpClient) {
        return new AdapterFactory(httpClient);
    }
}

package com.cidroid.core.di;

import com.cidroid.core.Application;
import com.cidroid.core.UserPreferences;
import dagger.Module;
import dagger.Provides;

import javax.inject.Singleton;

@Module
public class AppModule {
    private final Application mApplication;

    public AppModule(Application application) {
        mApplication = application;
    }

    @Provides
    @Singleton
    Application provideApplication() {
        return mApplication;
    }

    @Provides
    @Singleton
    UserPreferences provideUserPreferences(Application application) {
        return new UserPreferences(application);
    }
}

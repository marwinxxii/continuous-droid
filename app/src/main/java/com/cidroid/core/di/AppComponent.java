package com.cidroid.core.di;

import com.cidroid.adapters.AdapterFactory;
import com.cidroid.core.Application;
import com.cidroid.core.BuilderService;
import com.cidroid.core.UserPreferences;
import com.cidroid.core.db.DbOpenHelper;
import com.cidroid.core.db.ObservableDb;
import com.cidroid.ui.widget.ProjectListWidgetService;
import dagger.Component;

import javax.inject.Singleton;

@Component(modules = {AppModule.class, DatabaseModule.class, NetworkModule.class, AdapterModule.class})
@Singleton
public interface AppComponent {
    //share these objects with children components (sub-graphs)
    Application application();

    DbOpenHelper databaseHelper();

    ObservableDb observableDb();

    UserPreferences userPreferences();

    AdapterFactory adapterFactory();

    void inject(BuilderService builderService);

    void inject(ProjectListWidgetService.ProjectRemoteViewsFactory remoteViewsFactory);
}

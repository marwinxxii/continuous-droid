package com.cidroid.core.db;

import com.cidroid.core.RxHelper;
import com.cidroid.core.entities.Project;
import rx.Observable;
import rx.functions.Func0;
import rx.schedulers.Schedulers;

import java.util.List;
import java.util.concurrent.Callable;

public class ObservableDb {
    private final DbOpenHelper mDbOpenHelper;

    public ObservableDb(DbOpenHelper dbOpenHelper) {
        mDbOpenHelper = dbOpenHelper;
    }

    public Observable<List<Project>> getProjectList() {
        return observable(new Func0<List<Project>>() {
            @Override
            public List<Project> call() {
                return mDbOpenHelper.getProjectsDao().queryForAll();
            }
        });
    }

    private <T> Observable<T> observable(Callable<T> func) {
        return RxHelper.createObservable(func).subscribeOn(Schedulers.computation());
    }

    public Observable<Object> deleteProject(final Project project) {
        return observable(new Func0<Object>() {
            @Override
            public Object call() {
                mDbOpenHelper.getProjectsDao().deleteById(project.getId());
                mDbOpenHelper.getBuildStateDao().deleteById(project.getId());
                return "";
            }
        });
    }

    public Observable<Project> create(final Project project) {
        return observable(new Func0<Project>() {
            @Override
            public Project call() {
                mDbOpenHelper.getProjectsDao().create(project);
                return project;
            }
        });
    }
}

package com.cidroid.core.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.cidroid.core.entities.BuildStateDto;
import com.cidroid.core.entities.Project;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

public class DbOpenHelper extends OrmLiteSqliteOpenHelper {

    private static final int VERSION = 1;
    private static final String NAME = "cidroid.db";

    public DbOpenHelper(Context context) {
        super(context, NAME, null, VERSION);
    }

    public static DbOpenHelper getHelper(Context context) {
        return OpenHelperManager.getHelper(context, DbOpenHelper.class);
    }

    public RuntimeExceptionDao<Project, Integer> getProjectsDao() {
        return getRuntimeExceptionDao(Project.class);
    }

    public Dao<Project, Integer> getProjectsDao_() throws SQLException {
        return getDao(Project.class);
    }

    public RuntimeExceptionDao<BuildStateDto, Integer> getBuildStateDao() {
        return getRuntimeExceptionDao(BuildStateDto.class);
    }

    public Dao<BuildStateDto, Integer> getBuildStateDao_() throws SQLException {
        return getDao(BuildStateDto.class);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, Project.class);
            TableUtils.createTable(connectionSource, BuildStateDto.class);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
    }
}

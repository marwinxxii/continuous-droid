package com.cidroid.core.entities;

public class TriggerResult {
    private static final TriggerResult FAILURE = new TriggerResult(false, null);

    private final boolean isTriggerSuccess;
    private final String token;

    private TriggerResult(boolean success, String token) {
        this.isTriggerSuccess = success;
        this.token = token;
    }

    public static TriggerResult getSuccess(String token) {
        return new TriggerResult(true, token);
    }

    public static TriggerResult failure() {
        return FAILURE;
    }

    public boolean isTriggeredSuccessfully() {
        return isTriggerSuccess;
    }

    public String getToken() {
        return token;
    }
}

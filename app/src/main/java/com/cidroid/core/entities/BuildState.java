package com.cidroid.core.entities;

public enum BuildState {
    TRIGGERING, BUILDING, DOWNLOADING, SLEEPING
}

package com.cidroid.core.entities;

public enum BuildStatus {
    SUCCESS, FAILURE, NONE
}

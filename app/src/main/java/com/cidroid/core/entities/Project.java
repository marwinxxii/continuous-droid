package com.cidroid.core.entities;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

@DatabaseTable(tableName = "Projects")
public class Project {
    //TODO not generated
    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(canBeNull = false)
    private String name;

    @DatabaseField(canBeNull = false)
    private String displayName;

    @DatabaseField(canBeNull = false)
    private String url;

    @DatabaseField(canBeNull = false)
    private String apkDownloadUrl;

    @DatabaseField(canBeNull = false)
    private ProjectType projectType;

    @DatabaseField
    private Date lastBuildTime;

    @DatabaseField(canBeNull = false)
    private BuildStatus lastBuildStatus = BuildStatus.NONE;

    @DatabaseField
    private String lastBuildVersion;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String name) {
        this.displayName = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getApkDownloadUrl() {
        return apkDownloadUrl;
    }

    public void setApkDownloadUrl(String apkDownloadUrl) {
        this.apkDownloadUrl = apkDownloadUrl;
    }

    public ProjectType getProjectType() {
        return projectType;
    }

    public void setProjectType(ProjectType projectType) {
        this.projectType = projectType;
    }

    public Date getLastBuildTime() {
        return lastBuildTime;
    }

    public void setLastBuildTime(Date lastBuildTime) {
        this.lastBuildTime = lastBuildTime;
    }

    public BuildStatus getLastBuildStatus() {
        return lastBuildStatus;
    }

    public void setLastBuildStatus(BuildStatus lastBuildStatus) {
        this.lastBuildStatus = lastBuildStatus;
    }

    public String getLastBuildVersion() {
        return lastBuildVersion;
    }

    public void setLastBuildVersion(String lastBuildVersion) {
        this.lastBuildVersion = lastBuildVersion;
    }
}

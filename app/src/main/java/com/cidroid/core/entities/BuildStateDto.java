package com.cidroid.core.entities;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "BuildStatuses")
public class BuildStateDto {
    public static final String FIELD_DOWNLOAD_ID = "download_id";

    @DatabaseField(id = true)
    private int projectId;

    @DatabaseField
    private BuildState state;

    @DatabaseField
    private String token;

    @DatabaseField(columnName = FIELD_DOWNLOAD_ID)
    private long downloadId;

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public BuildState getState() {
        return state;
    }

    public void setState(BuildState state) {
        this.state = state;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public long getDownloadId() {
        return downloadId;
    }

    public void setDownloadId(long downloadId) {
        this.downloadId = downloadId;
    }
}

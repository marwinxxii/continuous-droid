package com.cidroid.core.net;

import com.cidroid.core.RxHelper;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import rx.Observable;
import rx.functions.Func0;
import rx.schedulers.Schedulers;

import java.io.IOException;

public class ObservableHttpClient {
    private final OkHttpClient mHttpClient;

    public ObservableHttpClient(OkHttpClient httpClient) {
        mHttpClient = httpClient;
    }

    public Observable<Response> execute(final Request request) {
        return RxHelper.createObservable(new Func0<Response>() {
            @Override
            public Response call() {
                try {
                    return mHttpClient.newCall(request).execute();
                    //TODO java.lang.Throwable: Explicit termination method 'end' not called
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }).subscribeOn(Schedulers.io());
    }
}

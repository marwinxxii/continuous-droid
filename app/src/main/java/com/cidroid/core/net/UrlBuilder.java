package com.cidroid.core.net;

import android.net.Uri;
import com.cidroid.core.entities.Project;
import com.squareup.okhttp.Request;

public class UrlBuilder {
    private Uri.Builder mUrBuilder;

    public UrlBuilder(Project project) {
        this(project.getUrl());
    }

    public UrlBuilder(String url) {
        mUrBuilder = Uri.parse(url).buildUpon();
    }

    public UrlBuilder appendEncodedPath(String path) {
        mUrBuilder.appendEncodedPath(path);
        return this;
    }

    public UrlBuilder appendQuery(String name, String value) {
        mUrBuilder.appendQueryParameter(name, value);
        return this;
    }

    public Request buildRequest() {
        return new Request.Builder().url(mUrBuilder.toString()).build();
    }

    public Request.Builder requestBuilder() {
        return new Request.Builder().url(mUrBuilder.toString());
    }
}

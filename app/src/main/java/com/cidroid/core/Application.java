package com.cidroid.core;

import android.os.StrictMode;
import com.cidroid.BuildConfig;
import com.cidroid.core.di.AppComponent;
import com.cidroid.core.di.AppModule;
import com.cidroid.core.di.Dagger_AppComponent;
import com.cidroid.utils.CrashLogger;

public class Application extends android.app.Application {
    private AppComponent mComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        CrashLogger.init(this);
        setStrictMode();
        mComponent = Dagger_AppComponent.builder()
          .appModule(new AppModule(this))
          .build();
    }

    private void setStrictMode() {
        if (BuildConfig.DEBUG) {
            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
              .detectAll()
              .penaltyLog()
                //.penaltyDeath()
              .build());

            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
              .detectAll()
              .penaltyLog()
              .penaltyDeath()
              .build());
        }
    }

    public AppComponent component() {
        return mComponent;
    }
}

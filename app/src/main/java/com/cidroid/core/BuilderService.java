package com.cidroid.core;

import android.app.*;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import com.cidroid.adapters.AdapterFactory;
import com.cidroid.core.db.DbOpenHelper;
import com.cidroid.core.entities.*;
import com.cidroid.ui.MainActivity;
import com.cidroid.utils.ActivityHelper;
import com.cidroid.utils.CrashLogger;
import com.cidroid.utils.DownloadHelper;
import com.cidroid.utils.IntentHelper;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import rx.Observable;
import rx.functions.Action1;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.inject.Inject;
import java.util.List;

import static com.cidroid.utils.LogHelper.LOGD;
import static com.cidroid.utils.LogHelper.LOGE;

public class BuilderService extends IntentService {
    private static final String NAME = "BuilderService";

    @Inject
    DbOpenHelper mDbHelper;

    @Inject
    UserPreferences mPreferences;

    @Inject
    AdapterFactory mAdapterFactory;

    private RuntimeExceptionDao<Project, Integer> mProjectDao;
    private RuntimeExceptionDao<BuildStateDto, Integer> mBuildStateDao;

    public BuilderService() {
        super(NAME);
    }

    public static Intent getStartBuildIntent(Context context, int projectId) {
        return getStartingIntent(context, projectId, Command.TRIGGER_BUILD);
    }

    private static Intent getStartingIntent(Context context, int projectId, Command command) {
        Intent intent = IntentHelper.createWithId(context, BuilderService.class, projectId);
        intent.setAction(command.name());
        return intent;
    }

    public static Intent getStartInstallIntent(Context context, Intent downloadIntent) {
        Intent intent = new Intent(context, BuilderService.class);
        intent.putExtras(downloadIntent);
        intent.setAction(Command.INSTALL.name());
        return intent;
    }

    public static Intent getStartDownloadIntent(Context context, int projectId) {
        return getStartingIntent(context, projectId, Command.DOWNLOAD_INSTALL);
    }

    private Observable<TriggerResult> triggerBuild(Project project, int maxRetries) {
        return mAdapterFactory.createForProject(project)
          .triggerBuild(project)
          .doOnNext(new Action1<TriggerResult>() {
              @Override
              public void call(TriggerResult triggerResult) {
                  if (!triggerResult.isTriggeredSuccessfully()) {
                      throw new RuntimeException("Failed to build");
                  }
              }
          })
            //TODO fix retry
            //TODO retry with delay
          .retry(maxRetries);
    }

    private Observable<BuildStatus> getBuildStatus(Project project, String token) {
        return mAdapterFactory.createForProject(project)
          .getBuildStatus(project, token);
    }

    public RuntimeExceptionDao<Project, Integer> getProjectDao() throws RuntimeException {
        if (mProjectDao == null) {
            mProjectDao = mDbHelper.getProjectsDao();
        }
        return mProjectDao;
    }

    public RuntimeExceptionDao<BuildStateDto, Integer> getBuildStateDao() throws RuntimeException {
        if (mBuildStateDao == null) {
            mBuildStateDao = mDbHelper.getBuildStateDao();
        }
        return mBuildStateDao;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        ((Application) getApplication()).component().inject(this);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            Command cmd = IntentHelper.getCommand(intent, Command.class);
            handleCommand(cmd, intent);
        } catch (RuntimeException e) {
            CrashLogger.logException(e);
        }
    }

    private void handleCommand(Command cmd, Intent intent) throws RuntimeException {
        switch (cmd) {
            case TRIGGER_BUILD:
                onTriggerBuild(intent);
                break;
            case CHECK_BUILD_STATUS:
                onCheckBuildStatus(intent);
                break;
            case INSTALL:
                onInstall(intent);
                break;
            case DOWNLOAD_INSTALL:
                onDownloadAndInstall(intent);
                break;
        }
    }

    @Nonnull
    private Project getProjectFromDb(int projectId) throws RuntimeException {
        Project project = getProjectDao().queryForId(projectId);
        if (project == null) {
            throw new RuntimeException("Project not found");
        }
        return project;
    }

    @Nullable
    private BuildStateDto getBuildStateFromDb(int projectId) throws RuntimeException {
        return getBuildStateDao().queryForId(projectId);
    }

    @Nonnull
    private Project loadProject(Intent intent) throws RuntimeException {
        int projectId;
        try {
            projectId = IntentHelper.getId(intent);
        } catch (IntentHelper.ExtraNotFoundException e) {
            throw new RuntimeException(e);
        }

        return getProjectFromDb(projectId);
    }

    private void onTriggerBuild(Intent intent) throws RuntimeException {
        Project project = loadProject(intent);
        BuildStateDto state = getBuildStateFromDb(project.getId());

        if (state == null) {
            state = new BuildStateDto();
            state.setState(BuildState.SLEEPING);
            state.setProjectId(project.getId());
        }

        NotificationManager nm = getNotificationManager();
        Notification.Builder notifBuilder = new Notification.Builder(this)
          .setContentIntent(PendingIntent.getActivity(this, 0, new Intent(this, MainActivity.class), PendingIntent.FLAG_UPDATE_CURRENT))
          .setSmallIcon(android.R.drawable.ic_media_play)
          .setAutoCancel(true)
          .setContentTitle(project.getName());

        if (!BuildState.SLEEPING.equals(state.getState())) {
            notifBuilder.setContentText("Not building, cause not sleep");
            nm.notify(1, notifBuilder.build());
            LOGE(NAME, "Not building, cause not sleep");//TODO notify
            return;
        }

        notifBuilder.setContentText("Triggering build");
        nm.notify(1, notifBuilder.build());
        LOGD(NAME, "triggering");
        state.setState(BuildState.TRIGGERING);
        getBuildStateDao().createOrUpdate(state);

        int maxRetries = mPreferences.getBuildRequestMaxRetries();
        //TODO non blocking?
        TriggerResult result = triggerBuild(project, maxRetries)
          .toBlocking().singleOrDefault(TriggerResult.failure());

        if (result.isTriggeredSuccessfully()) {
            state.setState(BuildState.BUILDING);
            state.setToken(result.getToken());
            LOGD(NAME, "scheduling");
            scheduleBuildStatusCheck(project);
            notifBuilder.setContentText("Waiting the app to be built");
            nm.notify(1, notifBuilder.build());
        } else {
            state.setState(BuildState.SLEEPING);
            //TODO notify about failure
            notifBuilder.setContentText("Could not trigger build");
            nm.notify(1, notifBuilder.build());
            LOGD(NAME, "failure");
        }
        getBuildStateDao().update(state);
    }

    private void onCheckBuildStatus(Intent intent) {
        Project project = loadProject(intent);
        BuildStateDto state = getBuildStateFromDb(project.getId());
        if (state == null) {
            throw new RuntimeException("State not found");
        }

        NotificationManager nm = getNotificationManager();
        Notification.Builder notifBuilder = new Notification.Builder(this)
          .setContentIntent(PendingIntent.getActivity(this, 0, new Intent(this, MainActivity.class), PendingIntent.FLAG_UPDATE_CURRENT))
          .setSmallIcon(android.R.drawable.ic_media_play)
          .setAutoCancel(true)
          .setContentTitle(project.getName());

        if (!BuildState.BUILDING.equals(state.getState())) {
            LOGE(NAME, "Not checking cause not building");
            return;
        }

        //TODO non blocking?
        BuildStatus status = getBuildStatus(project, state.getToken())
          .toBlocking().singleOrDefault(BuildStatus.NONE);

        switch (status) {
            case SUCCESS:
                LOGD(NAME, "cancelling scheduled");
                getPendingIntent(project, Command.CHECK_BUILD_STATUS).cancel();
                state.setState(BuildState.DOWNLOADING);
                LOGD(NAME, "downloading");
                long downloadId = enqueueDownload(project);
                state.setDownloadId(downloadId);
                getBuildStateDao().update(state);
                notifBuilder.setContentText("Downloading built APK");
                nm.notify(1, notifBuilder.build());
                //TODO notify about new version
                //WidgetHelper.notifyProjectsListChanged(this);
                break;
            case FAILURE:
                getPendingIntent(project, Command.CHECK_BUILD_STATUS).cancel();
                state.setState(BuildState.SLEEPING);
                getBuildStateDao().update(state);
                notifBuilder.setContentText("Build finished with failure");
                nm.notify(1, notifBuilder.build());
                break;
            case NONE:
                //TODO notification
                LOGD(NAME, "Not built yet");
                break;
        }
    }

    private void onInstall(Intent intent) throws RuntimeException {
        long downloadId = DownloadHelper.getDownloadIdFromIntent(intent);

        BuildStateDto state;
        List<BuildStateDto> states = getBuildStateDao().queryForEq(BuildStateDto.FIELD_DOWNLOAD_ID, downloadId);
        if (states.size() != 1) {
            throw new RuntimeException("More than 1 or 0 project found with download id");
        }
        state = states.get(0);

        DownloadManager.Query query = new DownloadManager.Query();
        query.setFilterById(downloadId);
        Cursor c = null;
        int status = DownloadManager.STATUS_FAILED;
        String uri = null;
        try {
            c = DownloadHelper.getManager(this).query(query);
            if (c.moveToNext()) {
                status = c.getInt(c.getColumnIndex(DownloadManager.COLUMN_STATUS));
                uri = c.getString(c.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI));
            } else {
                throw new RuntimeException("Download not found");
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }

        NotificationManager nm = getNotificationManager();
        Notification.Builder notifBuilder = new Notification.Builder(this)
          .setContentIntent(PendingIntent.getActivity(this, 0, new Intent(this, MainActivity.class), PendingIntent.FLAG_UPDATE_CURRENT))
          .setSmallIcon(android.R.drawable.ic_media_play)
          .setAutoCancel(true)
          .setContentTitle("Project install");

        if (status == DownloadManager.STATUS_SUCCESSFUL) {
            LOGD(NAME, "Downloaded:" + uri);
            notifBuilder.setContentText("Installing");
            nm.notify(1, notifBuilder.build());
            ActivityHelper.startInstallApkActivity(this, Uri.parse(uri));
        } else {
            LOGD(NAME, "download failed");
            notifBuilder.setContentText("Download of app failed. Sleeping.");
            nm.notify(1, notifBuilder.build());
            //TODO RETRY?
        }
        //state.setDownloadId(0);
        state.setState(BuildState.SLEEPING);

        getBuildStateDao().update(state);
    }

    private void onDownloadAndInstall(Intent intent) {
        Project project = loadProject(intent);
        BuildStateDto state = getBuildStateFromDb(project.getId());
        if (state == null) {
            state = new BuildStateDto();
            state.setState(BuildState.SLEEPING);
            state.setProjectId(project.getId());
        }

        if (!BuildState.SLEEPING.equals(state.getState())) {
            LOGE(NAME, "Not checking cause build in progress");
            return;
        }

        NotificationManager nm = getNotificationManager();
        Notification.Builder notifBuilder = new Notification.Builder(this)
          .setContentIntent(PendingIntent.getActivity(this, 0, new Intent(this, MainActivity.class), PendingIntent.FLAG_UPDATE_CURRENT))
          .setSmallIcon(android.R.drawable.ic_media_play)
          .setAutoCancel(true)
          .setContentTitle(project.getName());

        state.setState(BuildState.DOWNLOADING);
        long downloadId = enqueueDownload(project);
        state.setDownloadId(downloadId);
        getBuildStateDao().createOrUpdate(state);

        notifBuilder.setContentText("Downloading build");
        nm.notify(1, notifBuilder.build());
    }

    private PendingIntent scheduleBuildStatusCheck(Project project) {
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        long interval = mPreferences.getBuildCheckInterval();
        PendingIntent alarmIntent = getPendingIntent(project, Command.CHECK_BUILD_STATUS);
        alarmManager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME, interval, interval, alarmIntent);
        return alarmIntent;
    }

    private PendingIntent getPendingIntent(Project project, Command cmd) {
        Intent commandIntent = getStartingIntent(this, project.getId(), cmd);
        return PendingIntent.getService(this, 0, commandIntent, PendingIntent.FLAG_CANCEL_CURRENT);
    }

    private long enqueueDownload(Project project) {
        //TODO filename from URL
        String fileName = String.format("cid_%d_%s.apk", project.getId(), project.getName());
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(project.getApkDownloadUrl()));
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName);
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE);
        request.setTitle(project.getName());
        String desc = String.format("Built: %Tc", project.getLastBuildTime());
        request.setDescription(desc);
        return DownloadHelper.getManager(this).enqueue(request);
    }

    private NotificationManager getNotificationManager() {
        return (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
    }

    private static enum Command {
        TRIGGER_BUILD, CHECK_BUILD_STATUS, INSTALL, DOWNLOAD_INSTALL
    }
}

package com.cidroid.adapters;

import com.cidroid.core.entities.BuildStatus;
import com.cidroid.core.entities.Project;
import com.cidroid.core.entities.TriggerResult;
import rx.Observable;

//TODO declare throws exception
public interface IAdapter {
    //void setHttpClient(OkHttpClient client);
    Observable<Project> getProject(String url);

    //Observable<Project> refreshProjectStatus(Project project);
    Observable<TriggerResult> triggerBuild(Project project);

    Observable<BuildStatus> getBuildStatus(Project project, String token);

    //TODO cancel build
    //TODO set new project version after successful build
}

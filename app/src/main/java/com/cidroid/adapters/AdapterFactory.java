package com.cidroid.adapters;

import com.cidroid.adapters.ccnet.CCNetAdapter;
import com.cidroid.adapters.jenkins.JenkinsAdapter;
import com.cidroid.core.entities.Project;
import com.cidroid.core.entities.ProjectType;
import com.cidroid.core.net.ObservableHttpClient;

public class AdapterFactory {
    private final ObservableHttpClient mHttpClient;

    public AdapterFactory(ObservableHttpClient httpClient) {
        mHttpClient = httpClient;
    }

    public IAdapter createForProject(Project project) {
        return createForType(project.getProjectType());
    }

    public IAdapter createForType(ProjectType type) {
        BaseAdapter adapter;
        switch (type) {
            case CCNET:
                adapter = new CCNetAdapter();
                break;
            case JENKINS:
                adapter = new JenkinsAdapter();
                break;
            default:
                throw new IllegalArgumentException("Unknown project type");
        }
        adapter.setHttpClient(mHttpClient);
        return adapter;
    }
}

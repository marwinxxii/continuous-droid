package com.cidroid.adapters;

import com.cidroid.core.LocalizableRuntimeException;

public class ParseException extends LocalizableRuntimeException {
    public ParseException() {
        super();
    }

    public ParseException(String detailMessage) {
        super(detailMessage);
    }

    public ParseException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public ParseException(Throwable throwable) {
        super(throwable);
    }
}

package com.cidroid.adapters;

import com.cidroid.core.net.ObservableHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import rx.Observable;

public abstract class BaseAdapter implements IAdapter {
    private ObservableHttpClient mHttpClient;

    public final void setHttpClient(ObservableHttpClient httpClient) {
        mHttpClient = httpClient;
    }

    protected Observable<Response> execute(Request request) {
        return mHttpClient.execute(request);
    }
}

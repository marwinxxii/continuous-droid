package com.cidroid.adapters.ccnet;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamConverter;
import com.thoughtworks.xstream.converters.extended.ISO8601DateConverter;

import java.util.Date;

@XStreamAlias("Project")
public class CCNetProject {
    @XStreamAsAttribute
    public String name;

    @XStreamAsAttribute
    public String desc;

    @XStreamAsAttribute
    public ProjectActivity activity;

    @XStreamAsAttribute
    public String webUrl;

    @XStreamAsAttribute
    public IntegrationStatus lastBuildStatus;

    @XStreamAsAttribute
    @XStreamConverter(ISO8601DateConverter.class)
    public Date lastBuildTime;

    @XStreamAsAttribute
    public String lastBuildLabel;

    public String apkDownloadUrl;
}

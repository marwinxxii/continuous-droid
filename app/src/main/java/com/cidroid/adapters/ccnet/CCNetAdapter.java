package com.cidroid.adapters.ccnet;

import android.net.Uri;
import com.cidroid.R;
import com.cidroid.adapters.AdapterHelper;
import com.cidroid.adapters.BaseAdapter;
import com.cidroid.adapters.ParseException;
import com.cidroid.core.IncorrectArgumentException;
import com.cidroid.core.ProjectNotFoundException;
import com.cidroid.core.entities.BuildStatus;
import com.cidroid.core.entities.Project;
import com.cidroid.core.entities.TriggerResult;
import com.cidroid.core.net.UrlBuilder;
import com.cidroid.utils.I;
import com.cidroid.utils.RequestHelper;
import com.google.common.base.Predicate;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.thoughtworks.xstream.XStreamException;
import rx.Observable;
import rx.functions.Func1;

import java.io.InputStream;
import java.util.List;

public class CCNetAdapter extends BaseAdapter {
    private static final String XML_REPORT_PATH_FORMAT = "/ccnet/server/%1$s/XmlStatusReport.aspx";

    public static String getProjectUrlTemplate() {
        return "http://%1$s/ccnet/server/local/project/PROJ/ViewProjectReport.aspx";
    }

    @Override
    public Observable<Project> getProject(final String url) {
        return execute(buildProjectListUrl(url))
          .map(new Func1<Response, Project>() {
              @Override
              public Project call(Response response) {
                  return parseGetProjectResponse(url, response);
              }
          })
          .concatMap(new Func1<Project, Observable<? extends Project>>() {
              @Override
              public Observable<? extends Project> call(final Project project) {
                  return execute(RequestHelper.buildGet(project.getUrl()))
                    .map(new Func1<Response, Project>() {
                        @Override
                        public Project call(Response response) {
                            String url = findApkDownloadUrl(response.body().byteStream(), project.getUrl());
                            project.setApkDownloadUrl(url);
                            return project;
                        }
                    });
              }
          });
    }

    @Override
    public Observable<TriggerResult> triggerBuild(final Project project) {
        Request request = new UrlBuilder(project)
          .appendQuery("ForceBuild", "Force")
          .buildRequest();
        return execute(request).map(new Func1<Response, TriggerResult>() {
            @Override
            public TriggerResult call(Response response) {
                return parseTriggerBuildResponse(project, response);
            }
        });
    }

    private static TriggerResult parseTriggerBuildResponse(Project project, Response response)
      throws ProjectNotFoundException, ParseException {
        //TODO error data
        if (response.code() == 404) {
            throw new ProjectNotFoundException();
        } else if (response.code() >= 400) {
            throw new ParseException();
        }
        if (response.code() == 200) {
            String token = String.valueOf(project.getLastBuildTime().getTime());
            return TriggerResult.getSuccess(token);
        }
        //TODO 300 codes?
        return TriggerResult.failure();
    }

    @Override
    public Observable<BuildStatus> getBuildStatus(final Project project, final String token) {
        return execute(buildProjectListUrl(project.getUrl()))
          .map(new Func1<Response, BuildStatus>() {
              @Override
              public BuildStatus call(Response response) {
                  return parseBuildStatusResponse(project, token, response);
              }
          });
    }

    private static BuildStatus parseBuildStatusResponse(Project project, String token, Response response) throws ProjectNotFoundException, ParseException {
        CCNetProject result = parseProjectResult(project.getName(), response);
        long previousBuildTime = Long.parseLong(token);
        if (ProjectActivity.Sleeping.equals(result.activity)) {
            if (IntegrationStatus.Success.equals(result.lastBuildStatus)
              && result.lastBuildTime.getTime() > previousBuildTime) {
                return BuildStatus.SUCCESS;
            } else {
                return BuildStatus.FAILURE;
            }
        } else {
            return BuildStatus.NONE;
        }
    }

    public static Request buildProjectListUrl(String projectUrl) throws IllegalArgumentException {
        Uri uri = Uri.parse(projectUrl);
        List<String> segments = uri.getPathSegments();
        if (segments.size() < 4) {//because server is #3 and projectName is #4 in url
            throw new IncorrectArgumentException()
              .setMessageResId(R.string.error_project_incorrect_url);
        }
        String serverName = segments.get(2);
        String projectListUrl = uri.buildUpon()
          .path(String.format(XML_REPORT_PATH_FORMAT, serverName))
          .toString();

        return RequestHelper.buildGet(projectListUrl);
    }

    private static Project parseGetProjectResponse(String url, Response response) throws ProjectNotFoundException, ParseException {
        CCNetProject project = parseProjectResult(parseProjectNameFromUrl(url), response);
        Project result = new Project();
        result.setName(project.name);
        result.setDisplayName(project.name);
        result.setLastBuildStatus(project.lastBuildStatus.toCommon());
        result.setLastBuildTime(project.lastBuildTime);
        result.setUrl(project.webUrl);
        result.setLastBuildVersion(project.lastBuildLabel);
        return result;
    }

    public static String findApkDownloadUrl(InputStream stream, String projectUrl) {
        return AdapterHelper.findDefaultApkDownloadUrl(stream, projectUrl, ".ExternalLinks");
    }

    private static CCNetProject parseProjectResult(final String projectName, Response response)
      throws ProjectNotFoundException, ParseException {
        if (response.code() == 404) {
            throw new ProjectNotFoundException();
        } else if (response.code() >= 400) {
            throw new ParseException();
        }
        StatusReport report;
        try {
            report = Parser.parseStatusReport(response.body().byteStream());
        } catch (XStreamException e) {
            throw new ParseException(e);
        }
        CCNetProject result = I.firstOrNull(report.Projects, new Predicate<CCNetProject>() {
            @Override
            public boolean apply(CCNetProject ccNetProject) {
                return projectName.equals(ccNetProject.name);
            }
        });
        if (result == null) {
            throw new ProjectNotFoundException().setProjectName(projectName);
        }
        return result;
    }

    public static String parseProjectNameFromUrl(String url) {
        List<String> segments = Uri.parse(url).getPathSegments();
        return segments.get(segments.size() - 2);
    }
}

package com.cidroid.adapters.ccnet;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.ArrayList;

@XStreamAlias("Projects")
class StatusReport {
    @XStreamImplicit
    public ArrayList<CCNetProject> Projects;
}

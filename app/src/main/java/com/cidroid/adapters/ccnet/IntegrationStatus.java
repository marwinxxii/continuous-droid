package com.cidroid.adapters.ccnet;

enum IntegrationStatus {
    Success, Failure, Exception, Unknown, Cancelled;

    public com.cidroid.core.entities.BuildStatus toCommon() {
        switch (this) {
            case Success:
                return com.cidroid.core.entities.BuildStatus.SUCCESS;
            case Failure:
                return com.cidroid.core.entities.BuildStatus.FAILURE;
            default:
                return com.cidroid.core.entities.BuildStatus.NONE;
        }
    }
}

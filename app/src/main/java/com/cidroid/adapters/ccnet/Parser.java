package com.cidroid.adapters.ccnet;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.XStreamException;
import com.thoughtworks.xstream.io.xml.DomDriver;

import java.io.InputStream;

abstract class Parser {
    public static StatusReport parseStatusReport(InputStream stream) throws XStreamException {
        return (StatusReport) getXStream().fromXML(stream);
    }

    public static XStream getXStream() {
        XStream xStream = new XStream(new DomDriver());
        xStream.ignoreUnknownElements();
        xStream.autodetectAnnotations(true);
        xStream.processAnnotations(StatusReport.class);
        return xStream;
    }
}

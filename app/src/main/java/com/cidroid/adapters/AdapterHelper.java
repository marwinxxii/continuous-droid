package com.cidroid.adapters;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.io.InputStream;

public abstract class AdapterHelper {
    public static final String DOWNLOAD_LINK_TEXT = "Download application";

    public static String findDefaultApkDownloadUrl(String html, String selector) {
        return findDefaultApkDownloadUrl(Jsoup.parse(html), selector);
    }

    private static String findDefaultApkDownloadUrl(Document doc, String selector) {
        String url = null;
        for (Element link : doc.select(selector + " a")) {
            if (DOWNLOAD_LINK_TEXT.equals(link.text().trim())) {
                url = link.attr("href");
                break;
            }
        }
        return url;
    }

    public static String findDefaultApkDownloadUrl(InputStream stream, String url, String selector)
      throws ParseException {
        try {
            return findDefaultApkDownloadUrl(Jsoup.parse(stream, "utf-8", url), selector);
        } catch (IOException e) {
            throw new ParseException(e);
        }
    }
}

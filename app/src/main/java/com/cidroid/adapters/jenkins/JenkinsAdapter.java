package com.cidroid.adapters.jenkins;

import com.cidroid.adapters.AdapterHelper;
import com.cidroid.adapters.BaseAdapter;
import com.cidroid.adapters.ParseException;
import com.cidroid.core.ProjectNotFoundException;
import com.cidroid.core.entities.BuildStatus;
import com.cidroid.core.entities.Project;
import com.cidroid.core.entities.TriggerResult;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import okio.BufferedSink;
import rx.Observable;
import rx.functions.Func1;

import java.io.IOException;
import java.util.Date;

public class JenkinsAdapter extends BaseAdapter {
    private static final MediaType TEXT_PLAIN_MEDIA_TYPE = MediaType.parse("text/plain");
    private static final RequestBody EMPTY_PLAIN_TEXT_REQUEST_BODY = new RequestBody() {
        @Override
        public MediaType contentType() {
            return TEXT_PLAIN_MEDIA_TYPE;
        }

        @Override
        public void writeTo(BufferedSink sink) throws IOException {
        }
    };

    public static String getProjectUrlTemplate() {
        return "http://%1$s/job/PROJ";
    }

    @Override
    public Observable<Project> getProject(String url) {
        Request request = new UrlBuilder(url)
          .appendXmlApi()
          .appendQuery("depth", "1")
          .appendQuery("exclude", "//build")
          .buildRequest();

        return execute(request)
          .map(new Func1<Response, Project>() {
              @Override
              public Project call(Response response) {
                  return parseGetProject(response);
              }
          });
    }

    private static Project parseGetProject(Response response)
      throws ProjectNotFoundException, ParseException {
        if (response.code() == 404) {
            throw new ProjectNotFoundException();
        } else if (response.code() >= 400) {
            throw new ParseException();//TODO message
        }
        JenkinsProject project = Parser.parseProject(response.body().byteStream());
        Project result = new Project();
        result.setName(project.name);
        result.setDisplayName(project.displayName);
        result.setUrl(project.url);
        result.setApkDownloadUrl(findApkDownloadUrl(project.description));
        if (project.lastCompletedBuild != null) {
            result.setLastBuildStatus(project.lastCompletedBuild.result.toCommon());
            result.setLastBuildTime(new Date(project.lastCompletedBuild.timestamp));
        }
        return result;
    }

    private static String findApkDownloadUrl(String projectDescription) {
        return AdapterHelper.findDefaultApkDownloadUrl(projectDescription, "");
    }

    @Override
    public Observable<TriggerResult> triggerBuild(Project project) {
        Request request = new UrlBuilder(project)
          .appendEncodedPath("build")
          .requestBuilder()
          .post(EMPTY_PLAIN_TEXT_REQUEST_BODY)
          .build();

        return execute(request).map(new Func1<Response, TriggerResult>() {
            @Override
            public TriggerResult call(Response response) {
                return parseTriggerBuildResponse(response);
            }
        });
    }

    private static TriggerResult parseTriggerBuildResponse(Response response)
      throws ProjectNotFoundException, ParseException {
        if (response.code() == 404) {
            throw new ProjectNotFoundException();
        } else if (response.code() >= 400) {
            throw new ParseException();//TODO message
        }

        //TODO more reliable check requires extra request to build API, not just queue
        String queueItemLocation = response.header("Location");

        if (response.code() != 201 || queueItemLocation == null) {
            return TriggerResult.failure();
        }

        return TriggerResult.getSuccess(queueItemLocation);
    }

    @Override
    public Observable<BuildStatus> getBuildStatus(Project project, String token) {
        Request request = new UrlBuilder(token)
          .appendXmlApi()
          .appendQuery("depth", "1")
          .buildRequest();

        return execute(request).map(new Func1<Response, BuildStatus>() {
            @Override
            public BuildStatus call(Response response) {
                return parseBuildStatusResponse(response);
            }
        });
    }

    private static BuildStatus parseBuildStatusResponse(Response response)
      throws ProjectNotFoundException, ParseException {
        if (response.code() == 404) {
            throw new ProjectNotFoundException();
        } else if (response.code() >= 400) {
            throw new ParseException();//TODO message
        }
        QueueItem queueItem = Parser.parseQueueItem(response.body().byteStream());
        if (queueItem.build == null || queueItem.build.result == null) {
            return BuildStatus.NONE;//is being built
        }
        //check build numbers are equal?
        return queueItem.build.result.toCommon();
    }
}

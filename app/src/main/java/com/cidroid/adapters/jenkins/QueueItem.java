package com.cidroid.adapters.jenkins;

import com.thoughtworks.xstream.annotations.XStreamAlias;

class QueueItem {
    @XStreamAlias("executable")
    JenkinsBuild build;
}

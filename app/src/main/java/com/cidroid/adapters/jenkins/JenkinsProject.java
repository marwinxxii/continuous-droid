package com.cidroid.adapters.jenkins;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("freeStyleProject")
class JenkinsProject {
    String name;

    String displayName;

    String description;

    String url;

    boolean buildable;

    JenkinsBuild lastCompletedBuild;
    JenkinsBuild lastBuild;

}

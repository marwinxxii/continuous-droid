package com.cidroid.adapters.jenkins;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.XStreamException;
import com.thoughtworks.xstream.io.xml.DomDriver;

import java.io.InputStream;

abstract class Parser {
    public static JenkinsProject parseProject(InputStream stream) throws XStreamException {
        return (JenkinsProject) getXStream().fromXML(stream);
    }

    public static XStream getXStream() {
        XStream xStream = new XStream(new DomDriver());
        xStream.ignoreUnknownElements();
        xStream.autodetectAnnotations(true);
        xStream.processAnnotations(JenkinsProject.class);
        //when item is in build queue
        xStream.alias("waitingItem", QueueItem.class);
        //when item was built, but still present queue for some time
        xStream.alias("leftItem", QueueItem.class);
        xStream.processAnnotations(QueueItem.class);
        return xStream;
    }

    public static QueueItem parseQueueItem(InputStream stream) throws XStreamException {
        return (QueueItem) getXStream().fromXML(stream);
    }
}

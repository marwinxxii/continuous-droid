package com.cidroid.adapters.jenkins;

public enum BuildStatus {
    SUCCESS, FAILURE, ABORTED, NONE;

    public com.cidroid.core.entities.BuildStatus toCommon() {
        switch (this) {
            case SUCCESS:
                return com.cidroid.core.entities.BuildStatus.SUCCESS;
            case FAILURE:
                return com.cidroid.core.entities.BuildStatus.FAILURE;
            default:
                return com.cidroid.core.entities.BuildStatus.NONE;
        }
    }
}

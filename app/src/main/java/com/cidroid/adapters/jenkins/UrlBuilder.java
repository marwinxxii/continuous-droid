package com.cidroid.adapters.jenkins;

import com.cidroid.core.entities.Project;

class UrlBuilder extends com.cidroid.core.net.UrlBuilder {
    public UrlBuilder(Project project) {
        super(project);
    }

    public UrlBuilder(String url) {
        super(url);
    }

    public UrlBuilder appendXmlApi() {
        return (UrlBuilder) appendEncodedPath("api")
          .appendEncodedPath("xml");
    }
}

package com.cidroid.ui;

import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import com.cidroid.R;
import com.cidroid.core.BuilderService;
import com.cidroid.core.entities.BuildStatus;
import com.cidroid.core.entities.Project;
import com.cidroid.utils.DateHelper;

import static com.cidroid.utils.ViewHelper.toggleSetFormattedValue;

public class ProjectListItemView extends RelativeLayout {

    @InjectView(R.id.project_list_item_name)
    TextView mName;

    @InjectView(R.id.project_list_item_version)
    TextView mVersion;

    @InjectView(R.id.project_list_item_time)
    TextView mTime;

    @InjectView(R.id.project_list_item_build_status)
    TextView mBuildStatus;

    private int mProjectId;

    public ProjectListItemView(Context context) {
        super(context);
        initView(context);
    }

    private void initView(Context context) {
        LayoutInflater.from(context).inflate(R.layout.project_list_item, this, true);
        setLongClickable(true);
        ButterKnife.inject(this);
    }

    public ProjectListItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ProjectListItemView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        initView(getContext());
    }

    public void bindData(Project project) {
        mName.setText(project.getDisplayName());
        Resources res = getResources();

        toggleSetFormattedValue(mVersion, project.getLastBuildVersion());
        if (toggleSetFormattedValue(mBuildStatus, project.getLastBuildStatus())) {
            //TODO fix setting color
            if (BuildStatus.SUCCESS.equals(project.getLastBuildStatus())) {
                mBuildStatus.setTextColor(R.color.project_build_status_success);
            } else {
                mBuildStatus.setTextColor(R.color.project_build_status_failure);
            }
        }

        if (project.getLastBuildTime() != null) {
            CharSequence time = DateHelper.formatDateTimeRelative(getContext(), project.getLastBuildTime());
            mTime.setText(res.getString(R.string.project_list_item_time_label, time));
        } else {
            mTime.setText(R.string.project_not_build);
        }

        mProjectId = project.getId();
    }

    @OnClick(R.id.project_list_item_build)
    void onBuild() {
        getContext().startService(BuilderService.getStartBuildIntent(getContext(), mProjectId));
    }

    @OnClick(R.id.project_list_item_download)
    void onDownload() {
        getContext().startService(BuilderService.getStartDownloadIntent(getContext(), mProjectId));
    }
}

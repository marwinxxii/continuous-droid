package com.cidroid.ui;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import com.cidroid.R;

public class ToolbarActivity extends BaseActivity {
    Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected void setupToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.app_toolbar);
        setSupportActionBar(mToolbar);
    }
}

package com.cidroid.ui.di;

import android.app.Activity;
import dagger.Module;
import dagger.Provides;

@Module
public final class ActivityModule {
    private final Activity mActivity;

    public ActivityModule(Activity activity) {
        mActivity = activity;
    }

    @PerActivity
    @Provides
    Activity activity() {
        return mActivity;
    }
}

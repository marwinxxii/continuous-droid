package com.cidroid.ui.di;

import android.app.Activity;
import com.cidroid.core.di.AppComponent;
import com.cidroid.ui.AddProjectActivity;
import com.cidroid.ui.MainActivity;
import com.cidroid.ui.TaskFragment;
import dagger.Component;

@Component(dependencies = AppComponent.class, modules = ActivityModule.class)
@PerActivity
public interface ActivityComponent {
    Activity activity();

    void inject(MainActivity activity);

    void inject(AddProjectActivity activity);

    void inject(TaskFragment taskFragment);
}

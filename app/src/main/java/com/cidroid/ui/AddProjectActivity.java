package com.cidroid.ui;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.*;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.InjectViews;
import butterknife.OnClick;
import com.cidroid.R;
import com.cidroid.adapters.ccnet.CCNetAdapter;
import com.cidroid.adapters.jenkins.JenkinsAdapter;
import com.cidroid.core.IncorrectArgumentException;
import com.cidroid.core.ProjectNotFoundException;
import com.cidroid.core.UserPreferences;
import com.cidroid.core.db.ObservableDb;
import com.cidroid.core.entities.Project;
import com.cidroid.core.entities.ProjectType;
import com.cidroid.utils.CrashLogger;
import com.cidroid.utils.FragmentHelper;
import com.cidroid.utils.UriHelper;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

import javax.inject.Inject;
import java.net.ConnectException;
import java.net.UnknownHostException;
import java.util.List;

import static com.cidroid.core.Extras.getProjectType;
import static com.cidroid.core.Extras.putProjectType;
import static com.cidroid.utils.LogHelper.LOGD;
import static com.cidroid.utils.LogHelper.makeLogTag;

public class AddProjectActivity extends ToolbarActivity {
    private static final String TAG = makeLogTag(AddProjectActivity.class);
    private static final ButterKnife.Setter<View, Integer> VISIBILITY = new ButterKnife.Setter<View, Integer>() {
        @Override
        public void set(View view, Integer value, int index) {
            view.setVisibility(value);
        }
    };

    @Inject
    UserPreferences mPreferences;

    @Inject
    ObservableDb mObservableDb;

    @InjectView(R.id.project_add_url)
    EditText mUrl;

    @InjectView(R.id.project_add_save_server)
    CheckBox mSaveServer;

    @InjectView(R.id.project_add_button)
    Button mAddButton;

    @InjectView(R.id.project_add_type)
    Spinner mType;

    @InjectViews({R.id.project_add_url_label, R.id.project_add_url, R.id.project_add_save_server,
      R.id.project_add_button, R.id.project_add_type, R.id.project_add_type_label})
    List<View> mUrlViews;

    @InjectView(R.id.project_add_result_alias)
    EditText mResultAlias;

    @InjectView(R.id.project_add_result_download_url)
    EditText mResultDownloadUrl;

    @InjectViews({R.id.project_add_result_label, R.id.project_add_result_download_url,
      R.id.project_add_button_finish, R.id.project_add_result_alias, R.id.project_add_result_url_label})
    List<View> mResultViews;

    private TaskFragment mTaskFragment;
    private CompositeSubscription mSubscriptions = new CompositeSubscription();
    private ProjectType mProjectType;
    private Project mProject;

    public static Intent getStartIntent(Context context, ProjectType projectType) {
        return putProjectType(new Intent(context, AddProjectActivity.class), projectType);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
        setContentView(R.layout.project_add);
        setupToolbar();
        ButterKnife.inject(this);
        mProjectType = getProjectType(this);
        mTaskFragment = FragmentHelper.getOrCreate(this, TaskFragment.FACTORY, TaskFragment.withArgs(mProjectType));

        mType.setSelection(0);
        if (mPreferences.shouldSaveLastServerName()) {
            mSaveServer.setChecked(true);
        }

        mType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //TODO do not use position
                ProjectType type;
                if (position == 0) {
                    type = ProjectType.CCNET;
                } else {
                    type = ProjectType.JENKINS;
                }
                setUrlTemplate(type);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void setUrlTemplate(ProjectType type) {
        String lastServerName = mPreferences.getLastServerName();
        if (lastServerName == null || lastServerName.length() == 0) {
            lastServerName = "SERV";
        }
        String urlTemplate = "";
        if (ProjectType.CCNET.equals(type)) {
            urlTemplate = CCNetAdapter.getProjectUrlTemplate();
        } else if (ProjectType.JENKINS.equals(type)) {
            urlTemplate = JenkinsAdapter.getProjectUrlTemplate();
        }
        mUrl.setText(String.format(urlTemplate, lastServerName));
    }

    @Override
    protected void onStop() {
        super.onStop();
        mSubscriptions.unsubscribe();
    }

    @OnClick(R.id.project_add_button)
    void onAddProject() {
        //TODO validate url
        String url = UriHelper.prepareUrl(mUrl.getText().toString());

        String selectedType = (String) mType.getSelectedItem();
        ProjectType projectType;
        if (selectedType.equals(getString(R.string.project_add_type_ccnet))) {
            projectType = ProjectType.CCNET;
        } else {
            projectType = ProjectType.JENKINS;
        }

        mAddButton.setEnabled(false);
        mType.setEnabled(false);
        mUrl.setEnabled(false);
        mSubscriptions.add(mTaskFragment.observeLoadFromUrl(url, projectType)
          .observeOn(AndroidSchedulers.mainThread())
          .subscribe(
            new Action1<Project>() {
                @Override
                public void call(Project project) {
                    mProject = project;
                    hideViewGroup(mUrlViews);
                    mResultAlias.setText(project.getName());
                    mResultDownloadUrl.setText(project.getApkDownloadUrl());
                    showViewGroup(mResultViews);
                }
            },
            new Action1<Throwable>() {
                @Override
                public void call(Throwable throwable) {
                    LOGD(TAG, "exception", throwable);
                    CharSequence errorMessage;
                    if (throwable instanceof IncorrectArgumentException) {
                        IncorrectArgumentException ex = (IncorrectArgumentException) throwable;
                        if (ex.hasMessageResId()) {
                            errorMessage = getString(ex.getMessageResId());
                        } else {
                            errorMessage = getString(R.string.error_project_incorrect_url);
                        }
                    } else if (throwable instanceof ProjectNotFoundException) {
                        ProjectNotFoundException ex = (ProjectNotFoundException) throwable;
                        String projectName = ex.getProjectName();
                        errorMessage = getString(R.string.error_project_not_found, projectName != null ? projectName : "");
                    } else {
                        if (throwable.getCause() != null) {
                            Throwable cause = throwable.getCause();
                            if (cause instanceof ConnectException) {
                                errorMessage = getString(R.string.error_connection);
                            } else if (cause instanceof UnknownHostException) {
                                errorMessage = getString(R.string.error_unknown_host);
                            } else {
                                errorMessage = getString(R.string.error_project_find);
                            }
                        } else {
                            CrashLogger.logException(throwable);
                            //TODO log exception?
                            errorMessage = getString(R.string.error_project_find);
                        }
                    }
                    Toast.makeText(AddProjectActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
                    mAddButton.setEnabled(true);
                    mType.setEnabled(true);
                    mUrl.setEnabled(true);
                }
            }
          ));
    }

    private void hideViewGroup(List<View> views) {
        ButterKnife.apply(views, VISIBILITY, View.GONE);
    }

    private void showViewGroup(List<View> views) {
        ButterKnife.apply(views, VISIBILITY, View.VISIBLE);
    }

    @OnClick(R.id.project_add_button_finish)
    void onFinishAdding() {
        if (!TextUtils.isEmpty(mResultAlias.getText())) {
            mProject.setDisplayName(mResultAlias.getText().toString());
        }
        mSubscriptions.add(mObservableDb.create(mProject)
          .doOnNext(new Action1<Project>() {
              @Override
              public void call(Project project) {
                  String serverName = "";
                  if (mSaveServer.isChecked()) {
                      serverName = Uri.parse(project.getUrl()).getAuthority();
                  }
                  mPreferences.saveServerName(serverName);
              }
          })
          .observeOn(AndroidSchedulers.mainThread())
          .subscribe(new Subscriber<Object>() {
              @Override
              public void onCompleted() {
                  finish();
              }

              @Override
              public void onError(Throwable throwable) {
                  setResult(RESULT_CANCELED);
                  LOGD(TAG, "finish-error", throwable);
                  Toast.makeText(AddProjectActivity.this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
              }

              @Override
              public void onNext(Object o) {
                  setResult(RESULT_OK);
              }
          }));
    }
}

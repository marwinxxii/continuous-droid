package com.cidroid.ui;

import android.app.Fragment;
import android.os.Bundle;
import com.cidroid.adapters.AdapterFactory;
import com.cidroid.core.Extras;
import com.cidroid.core.entities.Project;
import com.cidroid.core.entities.ProjectType;
import com.cidroid.utils.FragmentHelper;
import rx.Observable;
import rx.functions.Action1;

import javax.inject.Inject;

public class TaskFragment extends Fragment {
    public static final String FRAGMENT_TAG = TaskFragment.class.getName();
    public static final FragmentHelper.IFactory<TaskFragment> FACTORY = new FragmentHelper.IFactory<TaskFragment>() {
        @Override
        public TaskFragment create() {
            return new TaskFragment();
        }

        @Override
        public String getTag() {
            return FRAGMENT_TAG;
        }
    };

    @Inject
    AdapterFactory mAdapterFactory;

    private ProjectType mProjectType;

    public static Bundle withArgs(ProjectType projectType) {
        return Extras.putProjectType(new Bundle(), projectType);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        ((BaseActivity) getActivity()).getActivityComponent().inject(this);
        mProjectType = Extras.getProjectType(this);
    }

    public Observable<Project> observeLoadFromUrl(final String url, final ProjectType projectType) {
        return mAdapterFactory.createForType(projectType)
          .getProject(url)
          .doOnNext(new Action1<Project>() {
              @Override
              public void call(Project project) {
                  project.setProjectType(projectType);
              }
          });
    }
}

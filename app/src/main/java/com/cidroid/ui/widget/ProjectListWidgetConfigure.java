package com.cidroid.ui.widget;

import android.app.Activity;
import android.appwidget.AppWidgetManager;
import android.os.Bundle;

public class ProjectListWidgetConfigure extends Activity {
    private int mAppWidgetId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            finish();
            return;
        }
        mAppWidgetId = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);

        if (mAppWidgetId == AppWidgetManager.INVALID_APPWIDGET_ID) {
            //TODO notify user
            finish();
            return;
        }


    }
}

package com.cidroid.ui.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.RemoteViews;
import com.cidroid.R;
import com.cidroid.core.BuilderService;
import com.cidroid.core.UserPreferences;
import com.cidroid.ui.MainActivity;
import com.cidroid.utils.CrashLogger;
import com.cidroid.utils.IntentHelper;

public class ProjectsAppWidgetProvider extends AppWidgetProvider {
    UserPreferences mPreferences;//TODO use injection
    public static Intent getBuildIntent(int projectId) {
        return getIntentWithId(Command.BUILD, projectId);
    }

    private static Intent getIntentWithId(Command cmd, int projectId) {
        return IntentHelper.putId(new Intent(cmd.name()), projectId);
    }

    public static Intent getShowProjectsIntent() {
        return new Intent(Command.SHOW_PROJECTS.name());
    }

    public static Intent getDownloadIntent(int projectId) {
        return getIntentWithId(Command.DOWNLOAD, projectId);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Command cmd = Command.NONE;
        try {
            cmd = IntentHelper.getCommand(intent, Command.class);
        } catch (IllegalArgumentException e) {
            //some other Android action
        }

        mPreferences = new UserPreferences(context);
        int projectId;
        switch (cmd) {
            case SHOW_PROJECTS:
                context.startActivity(IntentHelper.getIntentNewTask(context, MainActivity.class));
                break;
            case BUILD:
                try {
                    projectId = IntentHelper.getId(intent);
                    context.startService(BuilderService.getStartBuildIntent(context, projectId));
                } catch (IntentHelper.ExtraNotFoundException e) {
                    CrashLogger.logException(e);
                }
                break;
            case DOWNLOAD:
                try {
                    projectId = IntentHelper.getId(intent);
                    context.startService(BuilderService.getStartDownloadIntent(context, projectId));
                } catch (IntentHelper.ExtraNotFoundException e) {
                    CrashLogger.logException(e);
                }
                break;
            default:
                super.onReceive(context, intent);
                break;
        }
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        for (int appWidgetId : appWidgetIds) {
            Intent intent = new Intent(context, ProjectListWidgetService.class);
            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
            intent.setData(Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME)));

            RemoteViews rv = new RemoteViews(context.getPackageName(), R.layout.project_list_widget);
            rv.setRemoteAdapter(android.R.id.list, intent);
            rv.setEmptyView(android.R.id.list, android.R.id.empty);

            Intent onClickIntent = new Intent(context, ProjectsAppWidgetProvider.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, onClickIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            rv.setPendingIntentTemplate(android.R.id.list, pendingIntent);

            appWidgetManager.updateAppWidget(appWidgetId, rv);
        }
        mPreferences.addWidgetIds(appWidgetIds);
        super.onUpdate(context, appWidgetManager, appWidgetIds);
    }

    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        mPreferences.deleteWidgetIds(appWidgetIds);
        super.onDeleted(context, appWidgetIds);
    }

    private static enum Command {
        BUILD, SHOW_PROJECTS, DOWNLOAD, NONE
    }
}

package com.cidroid.ui.widget;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;
import com.cidroid.R;
import com.cidroid.core.Application;
import com.cidroid.core.db.DbOpenHelper;
import com.cidroid.core.entities.Project;
import com.cidroid.utils.DateHelper;

import javax.inject.Inject;
import java.util.List;

public class ProjectListWidgetService extends RemoteViewsService {
    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {
        return new ProjectRemoteViewsFactory((Application) getApplication(), intent);
    }

    public static class ProjectRemoteViewsFactory implements RemoteViewsFactory {
        @Inject
        DbOpenHelper mDbHelper;
        private Application mApplication;
        private int mAppWidgetId;
        private List<Project> mProjects;

        ProjectRemoteViewsFactory(Application application, Intent intent) {
            mApplication = application;
            mAppWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
        }

        @Override
        public void onCreate() {
            mApplication.component().inject(this);
        }

        @Override
        public void onDataSetChanged() {
            //TODO catch exception
            mProjects = mDbHelper.getProjectsDao().queryForAll();
        }

        @Override
        public void onDestroy() {
            mApplication = null;
            mProjects = null;
        }

        @Override
        public int getCount() {
            return mProjects.size();
        }

        @Override
        public RemoteViews getViewAt(int i) {
            Context context = mApplication.getApplicationContext();
            Project project = mProjects.get(i);
            RemoteViews rv = new RemoteViews(context.getPackageName(), R.layout.project_list_widget_item);
            rv.setTextViewText(R.id.project_list_widget_item_name, project.getDisplayName());
            CharSequence time;
            if (project.getLastBuildTime() != null) {
                time = DateHelper.formatDateTimeRelative(context, project.getLastBuildTime());
            } else {
                time = context.getString(R.string.project_not_build);
            }
            rv.setTextViewText(R.id.project_list_widget_item_time, time);

            Intent onClickIntent = ProjectsAppWidgetProvider.getShowProjectsIntent();
            rv.setOnClickFillInIntent(R.id.project_list_widget_item_name, onClickIntent);
            rv.setOnClickFillInIntent(R.id.project_list_widget_item_time, onClickIntent);

            Intent onBuildIntent = ProjectsAppWidgetProvider.getBuildIntent(project.getId());
            rv.setOnClickFillInIntent(R.id.project_list_widget_item_build, onBuildIntent);

            Intent onDownloadIntent = ProjectsAppWidgetProvider.getDownloadIntent(project.getId());
            rv.setOnClickFillInIntent(R.id.project_list_widget_item_download, onDownloadIntent);

            return rv;
        }

        @Override
        public RemoteViews getLoadingView() {
            return null;
        }

        @Override
        public int getViewTypeCount() {
            return 1;
        }

        @Override
        public long getItemId(int i) {
            return mProjects.get(i).getName().hashCode();
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }
    }
}

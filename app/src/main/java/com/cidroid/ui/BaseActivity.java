package com.cidroid.ui;

import android.support.v7.app.ActionBarActivity;
import com.cidroid.core.Application;
import com.cidroid.ui.di.ActivityComponent;
import com.cidroid.ui.di.ActivityModule;
import com.cidroid.ui.di.Dagger_ActivityComponent;

public class BaseActivity extends ActionBarActivity {
    private ActivityComponent mActivityComponent;

    public ActivityComponent getActivityComponent() {
        if (mActivityComponent == null) {
            mActivityComponent = Dagger_ActivityComponent.builder()
              .appComponent(((Application) getApplication()).component())
              .activityModule(new ActivityModule(this))
              .build();
        }
        return mActivityComponent;
    }
}

package com.cidroid.ui;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import com.cidroid.core.entities.Project;

import java.util.List;

public class ProjectListAdapter extends ArrayAdapter<Project> {

    public ProjectListAdapter(Context context, List<Project> projects) {
        super(context, 0, projects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ProjectListItemView view;
        if (convertView == null) {
            view = new ProjectListItemView(getContext());
        } else {
            view = (ProjectListItemView) convertView;
        }
        view.bindData(getItem(position));
        return view;
    }
}

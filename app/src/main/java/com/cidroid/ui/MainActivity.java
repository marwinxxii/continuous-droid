package com.cidroid.ui;

import android.appwidget.AppWidgetManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import com.cidroid.R;
import com.cidroid.core.RxHelper;
import com.cidroid.core.UserPreferences;
import com.cidroid.core.db.DbOpenHelper;
import com.cidroid.core.db.ObservableDb;
import com.cidroid.core.entities.BuildStatus;
import com.cidroid.core.entities.Project;
import com.cidroid.core.entities.ProjectType;
import com.cidroid.utils.DialogHelper;
import com.cidroid.utils.FileStorageHelper;
import com.cidroid.utils.ToastHelper;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func0;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

import javax.inject.Inject;
import java.io.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;

public class MainActivity extends ToolbarActivity {
    private static final char CSV_DELIMITER = ',';
    private static final String CSV_FILE_NAME = "cidroid-projects.csv";

    @Inject
    DbOpenHelper mDbOpenHelper;//TODO remove after good import created

    @Inject
    UserPreferences mPreferences;

    @Inject
    ObservableDb mObservableDb;

    private CompositeSubscription mSubscriptions;
    private ListView mListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
        setTitle(R.string.project_list_activity_title);
        setContentView(R.layout.project_list);
        setupToolbar();

        mListView = (ListView) findViewById(android.R.id.list);
        registerForContextMenu(mListView);
        mListView.setAdapter(new ProjectListAdapter(this, new ArrayList<Project>()));
    }

    @Override
    protected void onStop() {
        super.onStop();
        mSubscriptions.unsubscribe();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case R.id.main_menu_add_project:
                startAddProjectActivity();
                return true;
            case R.id.main_menu_export_projects:
                exportProjects();
                return true;
            case R.id.main_menu_import_projects:
                importProjects();
                return true;
            case R.id.main_menu_reset:
                mSubscriptions.add(RxHelper.createObservable(new Func0<Void>() {
                    @Override
                    public Void call() {
                        try {
                            mDbOpenHelper
                              .getBuildStateDao()
                              .deleteBuilder()
                              .delete();
                            return null;
                        } catch (SQLException e) {
                            throw new RuntimeException(e);
                        }
                    }
                })
                  .subscribeOn(Schedulers.computation())
                  .subscribe(new Action1<Void>() {
                      @Override
                      public void call(Void aVoid) {
                          Toast.makeText(MainActivity.this, "State reset", Toast.LENGTH_SHORT).show();
                      }
                  }, new Action1<Throwable>() {
                      @Override
                      public void call(Throwable throwable) {
                          Toast.makeText(MainActivity.this,
                            "Error resetting" + throwable.getMessage(), Toast.LENGTH_SHORT).show();
                      }
                  }));
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        getMenuInflater().inflate(R.menu.project_list_item, menu);
    }

    @Override
    public boolean onContextItemSelected(final MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        final Project project = getListAdapter().getItem(info.position);
        switch (item.getItemId()) {
            /*case R.id.rename:
                return true;*/
            case R.id.delete:
                DialogHelper.createDeleteConfirmationDialog(this, project,
                  new DialogInterface.OnClickListener() {
                      @Override
                      public void onClick(DialogInterface dialog, int which) {
                          onDeleteProject(project);
                      }
                  })
                  .show();
                return true;
        }
        return super.onContextItemSelected(item);
    }

    private void onDeleteProject(final Project project) {
        mSubscriptions.add(
          mObservableDb.deleteProject(project)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Subscriber<Object>() {
                @Override
                public void onCompleted() {
                }

                @Override
                public void onError(Throwable e) {
                    Toast.makeText(MainActivity.this, R.string.error_project_delete, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onNext(Object object) {
                    getListAdapter().remove(project);
                    getListAdapter().notifyDataSetChanged();
                    notifyProjectListChanged();
                }
            })
        );
    }

    private void notifyProjectListChanged() {
        int[] widgetIds = mPreferences.getWidgetIds();
        if (widgetIds != null) {
            AppWidgetManager.getInstance(this).notifyAppWidgetViewDataChanged(widgetIds, android.R.id.list);
        }
    }

    private void startAddProjectActivity() {
        startActivityForResult(AddProjectActivity.getStartIntent(this, ProjectType.CCNET), 100);
    }

    //TODO refactor export
    private void exportProjects() {
        mSubscriptions.add(
          mObservableDb.getProjectList()
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext(new Action1<List<Project>>() {
                @Override
                public void call(List<Project> projects) {
                    writeProjectsToFile(projects);
                }
            })
            .subscribe(
              new Action1<List<Project>>() {
                  @Override
                  public void call(List<Project> projects) {
                      ToastHelper.showShortToast(MainActivity.this, R.string.export_success);
                  }
              },
              new Action1<Throwable>() {
                  @Override
                  public void call(Throwable throwable) {
                      ToastHelper.showShortToast(MainActivity.this, R.string.export_error_write);
                  }
              }
            )
        );
    }

    //TODO refactor import, add validation, clear list->import->add
    private void importProjects() {
        mSubscriptions.add(
          RxHelper.createObservable(new Callable<ArrayList<Project>>() {
              @Override
              public ArrayList<Project> call() {
                  File input = getCsvFile();
                  RuntimeExceptionDao<Project, Integer> dao = mDbOpenHelper.getProjectsDao();
                  ArrayList<Project> result = new ArrayList<>();
                  try {
                      BufferedReader reader = new BufferedReader(new FileReader(input));
                      String line;
                      while ((line = reader.readLine()) != null) {
                          String[] fields = line.split(",");

                          Project project = new Project();
                          project.setId(Integer.valueOf(fields[0], 10));
                          project.setName(fields[1]);
                          project.setDisplayName(fields[2]);
                          project.setUrl(fields[3]);
                          project.setApkDownloadUrl(fields[4]);
                          project.setProjectType(ProjectType.valueOf(fields[5]));
                          project.setLastBuildTime(new Date(Long.valueOf(fields[6])));
                          project.setLastBuildStatus(BuildStatus.valueOf(fields[7]));
                          project.setLastBuildVersion(fields[8]);

                          dao.create(project);
                          result.add(project);
                      }
                      reader.close();
                      mDbOpenHelper.getBuildStateDao().deleteBuilder().delete();
                  } catch (FileNotFoundException e) {
                      throw new RuntimeException(e);
                  } catch (IOException e) {
                      throw new RuntimeException(e);
                  } catch (SQLException e) {
                      throw new RuntimeException(e);
                  }
                  return result;
              }
          })
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
              new Subscriber<ArrayList<Project>>() {
                  @Override
                  public void onCompleted() {
                      ToastHelper.showShortToast(MainActivity.this, R.string.import_success);
                  }

                  @Override
                  public void onError(Throwable throwable) {
                      int resId = R.string.import_error;
                      if (throwable.getCause() != null && throwable.getCause() instanceof FileNotFoundException) {
                          resId = R.string.import_error_file_not_found;
                      }
                      ToastHelper.showShortToast(MainActivity.this, resId);
                  }

                  @Override
                  public void onNext(ArrayList<Project> projects) {
                      getListAdapter().clear();
                      getListAdapter().addAll(projects);
                      getListAdapter().notifyDataSetChanged();
                  }
              }
            )
        );
    }

    private static void writeProjectsToFile(List<Project> projects) {
        StringBuilder csvBuilder = new StringBuilder();
        for (Project project : projects) {
            csvBuilder.append(project.getId())
              .append(CSV_DELIMITER)
              .append(project.getName())
              .append(CSV_DELIMITER)
              .append(project.getDisplayName())
              .append(CSV_DELIMITER)
              .append(project.getUrl())
              .append(CSV_DELIMITER)
              .append(project.getApkDownloadUrl())
              .append(CSV_DELIMITER)
              .append(project.getProjectType().name())
              .append(CSV_DELIMITER)
              .append(project.getLastBuildTime().getTime())
              .append(CSV_DELIMITER)
              .append(project.getLastBuildStatus().name())
              .append(CSV_DELIMITER)
              .append(project.getLastBuildVersion())
              .append('\n');
        }
        File result = getCsvFile();
        try {
            FileWriter writer = new FileWriter(result);
            writer.write(csvBuilder.toString());
            writer.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static File getCsvFile() {
        return new File(FileStorageHelper.getDownloadsDir(), CSV_FILE_NAME);
    }

    public ProjectListAdapter getListAdapter() {
        return (ProjectListAdapter) mListView.getAdapter();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            //TODO do not reload whole list
            loadProjectsList();
            notifyProjectListChanged();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mSubscriptions = new CompositeSubscription();
        loadProjectsList();
    }

    private void loadProjectsList() {
        //TODO handle errors
        mSubscriptions.add(
          mObservableDb.getProjectList()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Action1<List<Project>>() {
                @Override
                public void call(List<Project> projects) {
                    getListAdapter().clear();
                    getListAdapter().addAll(projects);
                    getListAdapter().notifyDataSetChanged();
                }
            })
        );
    }
}

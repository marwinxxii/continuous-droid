package com.cidroid.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import com.cidroid.R;
import com.cidroid.core.entities.Project;

public abstract class DialogHelper {
    public static AlertDialog createDeleteConfirmationDialog(Context context, Project project,
      DialogInterface.OnClickListener onDelete) {
        return new AlertDialog.Builder(context)
          .setCancelable(true)
          .setPositiveButton(R.string.delete, onDelete)
          .setIcon(android.R.drawable.ic_dialog_alert)
          .setNegativeButton(android.R.string.cancel, null)
          .setTitle(R.string.project_dialog_delete_title)
          .setMessage(context.getString(R.string.project_dialog_delete_message, project.getDisplayName()))
          .create();
    }
}

package com.cidroid.utils;

import android.content.Context;
import android.text.format.DateUtils;

import java.util.Date;

public abstract class DateHelper {
    public static CharSequence formatDateTimeRelative(Context c, Date dateTime) {
        return DateUtils.getRelativeDateTimeString(c, dateTime.getTime(), DateUtils.MINUTE_IN_MILLIS, DateUtils.DAY_IN_MILLIS, DateUtils.FORMAT_ABBREV_ALL);
    }
}

package com.cidroid.utils;

import android.net.Uri;

public abstract class UriHelper {
    public static String prepareUrl(String url) {
        //TODO refactor uri preparing
        Uri uri = Uri.parse(url);
        return new Uri.Builder()
          .scheme(uri.getScheme())
          .encodedAuthority(uri.getAuthority())
          .path(uri.getPath())
          .query(uri.getQuery())
          .fragment(uri.getFragment())
          .toString();
    }
}

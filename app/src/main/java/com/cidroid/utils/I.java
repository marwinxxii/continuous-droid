package com.cidroid.utils;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;

import javax.annotation.Nullable;
import java.util.Collection;

public abstract class I {
    @Nullable
    public static <T> T firstOrNull(Collection<T> collection, Predicate<T> predicate) {
        Iterable<T> filtered = Iterables.filter(collection, predicate);
        T result = null;
        int count = 0;
        for (T obj : filtered) {
            result = obj;
            count++;
        }
        if (count > 1) {
            throw new IllegalArgumentException("More than one item found in collection");
        }
        return result;
    }
}

package com.cidroid.utils;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;

public abstract class FragmentHelper {

    public static <T extends Fragment> T getOrCreate(Activity activity, IFactory<T> factory) {
        return getOrCreate(activity, factory, null);
    }

    public static <T extends Fragment> T getOrCreate(Activity activity, IFactory<T> factory, Bundle args) {
        FragmentManager fm = activity.getFragmentManager();
        T fr = (T) fm.findFragmentByTag(factory.getTag());
        if (fr == null) {
            fr = factory.create();
            if (args != null) {
                fr.setArguments(args);
            }
            fm.beginTransaction().add(fr, factory.getTag()).commit();
        }
        return fr;
    }

    public static interface IFactory<T extends Fragment> {
        T create();

        String getTag();
    }
}

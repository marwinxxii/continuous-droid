package com.cidroid.utils;

import android.net.Uri;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;

public abstract class RequestHelper {
    public static Request buildGet(Uri.Builder builder) {
        return buildGet(builder.toString());
    }

    public static Request buildGet(String url) {
        return new Request.Builder().url(url).build();
    }

    public static Request buildPost(Uri.Builder builder, RequestBody body) {
        return buildPost(builder.toString(), body);
    }

    public static Request buildPost(String url, RequestBody body) {
        return new Request.Builder().url(url).post(body).build();
    }
}

package com.cidroid.utils;

import static com.cidroid.utils.LogHelper.LOGD;

public abstract class DebugHelper {
    public static void sleep(long ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            LOGD("DebugHelper", "sleep", e);
        }
    }
}

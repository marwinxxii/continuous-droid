package com.cidroid.utils;

import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;

public abstract class DownloadHelper {
    public static DownloadManager getManager(Context context) {
        return (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
    }

    public static long getDownloadIdFromIntent(Intent intent) throws IllegalArgumentException {
        long id = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
        if (id <= 0) {
            throw new IllegalArgumentException("Incorrect download id");
        }
        return id;
    }
}

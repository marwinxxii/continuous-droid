package com.cidroid.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import java.io.File;

public abstract class ActivityHelper {
    private static final String APP_MIME_TYPE = "application/vnd.android.package-archive";

    public static void startInstallApkActivity(Context context, Uri fileUri) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setDataAndType(fileUri, APP_MIME_TYPE);
        context.startActivity(intent);
    }

    public static void startInstallApkActivity(Context context, File file) {
        startInstallApkActivity(context, Uri.fromFile(file));
    }
}

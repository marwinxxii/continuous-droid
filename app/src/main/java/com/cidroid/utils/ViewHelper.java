package com.cidroid.utils;

import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

public abstract class ViewHelper {
    /**
     * Show TextView and set text if text is not empty.
     *
     * @param view to set visibility and text
     * @param text to check for empty
     * @return true if view is shown
     */
    public static boolean toggleSetValue(TextView view, CharSequence text) {
        if (TextUtils.isEmpty(text)) {
            view.setVisibility(View.GONE);
            return false;
        } else {
            view.setText(text);
            view.setVisibility(View.VISIBLE);
            return true;
        }
    }

    public static boolean toggleSetFormattedValue(TextView view, Object value) {
        if (value != null) {
            String labelFormat = (String) view.getTag();
            view.setText(String.format(labelFormat, value));
            view.setVisibility(View.VISIBLE);
            return true;
        } else {
            view.setVisibility(View.GONE);
            return false;
        }
    }
}

package com.cidroid.utils;

import android.content.Context;
import com.cidroid.BuildConfig;
import com.crashlytics.android.Crashlytics;

import static com.cidroid.utils.LogHelper.LOGE;
import static com.cidroid.utils.LogHelper.makeLogTag;

public abstract class CrashLogger {
    private static final String TAG = makeLogTag(CrashLogger.class);

    public static void init(Context context) {
        if (BuildConfig.REPORT_CRASHES) {
            Crashlytics.start(context);
        }
    }

    public static void logException(Throwable error) {
        if (BuildConfig.REPORT_CRASHES) {
            Crashlytics.logException(error);
        } else {
            LOGE(TAG, "log exception", error);
        }
    }
}

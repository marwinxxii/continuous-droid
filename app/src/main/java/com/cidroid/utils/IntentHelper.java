package com.cidroid.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

public abstract class IntentHelper {
    public static final String EXTRA_ID = "id";

    public static Intent putId(Intent intent, int id) {
        intent.putExtra(EXTRA_ID, id);
        return intent;
    }

    public static Intent createWithId(Context context, Class<?> cl, int id) {
        return putId(new Intent(context, cl), id);
    }

    public static int getId(Intent intent) throws ExtraNotFoundException {
        int id = intent.getIntExtra(EXTRA_ID, -1);
        if (id <= 0) {
            throw new ExtraNotFoundException("ID not found in intent");
        }
        return id;
    }

    public static Intent getIntentNewTask(Context context, Class<? extends Activity> cl) {
        Intent intent = new Intent(context, cl);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        return intent;
    }

    public static <T extends Enum> T getCommand(Intent intent, Class<T> cl) throws IllegalArgumentException {
        String action = intent.getAction();
        if (action == null || action.isEmpty()) {
            throw new IllegalArgumentException("Action is null or empty");
        }
        return (T) Enum.valueOf(cl, action);
    }

    public static class ExtraNotFoundException extends Exception {
        public ExtraNotFoundException(String message) {
            super(message);
        }
    }
}

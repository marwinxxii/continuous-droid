package com.cidroid.utils;

import android.os.Environment;

import java.io.File;

public abstract class FileStorageHelper {
    public static File getDownloadsDir() {
        return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
    }
}

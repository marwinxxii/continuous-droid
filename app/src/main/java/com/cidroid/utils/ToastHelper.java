package com.cidroid.utils;

import android.content.Context;
import android.widget.Toast;

public abstract class ToastHelper {
    public static void showShortToast(Context context, int resId) {
        Toast.makeText(context, resId, Toast.LENGTH_SHORT).show();
    }
}

# ContinuousDroid

This application helps quickly build and install Android applcations from
your continuous integration server like Jenkins and CruiseControl.Net.

[App on Google.Play](https://play.google.com/store/apps/details?id=com.cidroid)

## Configuring Jenkins

At the moment, project, building Android app, should of type 'freeStyle'.

1. [Enable raw HTML in project description](http://stackoverflow.com/a/24864804/905565)
2. Add following link so ContinuousDroid could find your built APK:
~~~
<a href="http://192.168.1.7:8080/job/Continuous%20Droid-Dev/ws/app/build/apk/app-debug-unaligned.apk">Download application</a>
~~~
(replace URL with yours, I used link to APK in workspace).

## Configuring CruiseControl.NET

In order to APK links to be found by ContinuousDroid you need to add to
you project external link pointing to result APK and text 'Download application'.

~~~
<externalLinks>
    <externalLink>http://192.168.1.8/artifacts/ContinuousDroid.apk</externalLink>
</externalLinks>
~~~

## License

GPL v3, see LICENSE.txt.